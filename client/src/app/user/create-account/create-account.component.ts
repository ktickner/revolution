import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService, UserService } from "../../shared/services";

import { Errors, User } from "../../shared/interfaces";

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})

export class CreateAccountComponent implements OnInit {
  private accountForm: FormGroup;

  private errors: Errors = {
    general: '',
    email: '',
    password: '',
    passwordConfirm: '',
  };

  private validationMessages: object = {
    email: {
      required: 'Your email address is required',
    },
    password: {
      required: 'Your password is required',
      minlength: 'Your password needs to be at least 8 characters long',
    },
    passwordConfirm: {
      required: 'Please confirm your password',
      validateEqual: 'Your password confirmation doesn\'t match',
    },
  };

  constructor(
    private FormBuilder: FormBuilder,
    private Router: Router,
    private AuthService: AuthService,
    private UserService: UserService,
  ) {
    this.accountForm = this.FormBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [
        Validators.required,
        Validators.minLength(8),
      ]],
      passwordConfirm: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.accountForm.valueChanges.subscribe((): void => this.onValueChanged(this.accountForm));

    this.onValueChanged(this.accountForm);
  }

  onValueChanged(form: FormGroup): void {
    for (const field in this.errors) {
      this.errors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];

        for (const key in control.errors) {
          this.errors[field] += `${messages[key]} `;
        }
      }
    }
  }

  onSubmit({ value, valid }: { value: User, valid: boolean }): void {
    if (!valid) {
      return;
    }

    this.UserService.createUser(value).subscribe((response: User): void => {
      this.Router.navigate(['/unconfirmed-account'], { queryParams: { userId: response.id } });
    }, (error): void => {
      const errors = error.error.error;

      if (error.status === 0 || !errors || errors.length === 0) {
        this.errors.general = 'There is a problem on the server. Please try again later';
      }

      errors.forEach((errorObject) => {
        if (!errorObject.field) {
          errorObject.field = 'general';
        }

        this.errors[errorObject.field] = `${errorObject.message}`;
      });
    });
  }
}
