import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";

import { SharedModule } from "../shared";

import { UserRoutingModule } from "./user-routing.module";

import { UserComponent } from "./user.component";
import { CreateAccountComponent } from "./create-account/create-account.component";
import { ForgottenPasswordComponent } from "./forgotten-password/forgotten-password.component";
import { InactiveAccountComponent } from "./inactive-account/inactive-account.component";
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { LoginComponent } from "./login/login.component";
import { UnconfirmedAccountComponent } from "./unconfirmed-account/unconfirmed-account.component";

@NgModule({
  declarations: [
    UserComponent,
    CreateAccountComponent,
    ForgottenPasswordComponent,
    InactiveAccountComponent,
    LandingPageComponent,
    LoginComponent,
    UnconfirmedAccountComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UserRoutingModule,
    SharedModule,
  ],
  exports: [
    UserRoutingModule,
  ],
})

export class UserModule { }
