import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

import { Subscription } from "rxjs/Subscription";

import { Errors, Session, User } from "../../shared/interfaces";

import { UserService } from "../../shared/services";

@Component({
  selector: 'app-forgotten-password',
  templateUrl: './forgotten-password.component.html',
  styleUrls: ['./forgotten-password.component.scss']
})

export class ForgottenPasswordComponent implements OnInit, OnDestroy {
  private forgottenPasswordForm: FormGroup;
  private passwordResetForm: FormGroup;

  private paramsSub: Subscription;
  private emailSent: boolean;
  private passwordReset: boolean;
  private token: string;

  private errors: Errors = {
    general: '',
    email: '',
    password: '',
    passwordConfirm: '',
  };

  private validationMessages: Object = {
    email: {
      required: 'Your email address is required',
    },
    password: {
      required: 'Your new password is required',
      minlength: 'Your password needs to be at least 8 characters long',
    },
    passwordConfirm: {
      required: 'Please confirm your new password',
      validateEqual: 'Your password confirmation doesn\'t match',
    },
  };

  constructor(
    private ActivatedRoute: ActivatedRoute,
    private FormBuilder: FormBuilder,
    private Router: Router,
    private UserService: UserService,
  ) {
    this.forgottenPasswordForm = this.FormBuilder.group({
      email: ['', Validators.required],
    });

    this.passwordResetForm = this.FormBuilder.group({
      password: ['', [
        Validators.required,
        Validators.minLength(8),
      ]],
      passwordConfirm: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.emailSent = false;
    this.passwordReset = false;

    this.paramsSub = this.ActivatedRoute.queryParams.subscribe((params: object): void => {
      this.token = params['token'] || '';
    });

    this.forgottenPasswordForm.valueChanges.subscribe((): void => this.onValueChanged(this.forgottenPasswordForm));
    this.passwordResetForm.valueChanges.subscribe((): void => this.onValueChanged(this.passwordResetForm));
  }

  ngOnDestroy(): void {
    this.paramsSub.unsubscribe();
  }

  onValueChanged(form: FormGroup): void {
    this.resetErrors();

    for (const field in this.errors) {
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];

        for (const key in control.errors) {
          this.errors[field] += `${messages[key]} `;
        }
      }
    }
  }

  onForgottenSubmit({ value, valid }: { value: Session, valid: boolean }): void {
    if (!valid) {
      return;
    }

    const params: { email: string, type: string } = {
      email: value.email,
      type: 'passwordReset',
    };

    this.UserService.sendEmail(params).subscribe((response: Session): void => {
      this.emailSent = true;
      this.token = response.token;
    }, (error): void => {
      const errors = error.error.error;

      if (error.status === 0 || !errors || errors.length === 0) {
        this.errors.general = 'There is a problem on the server. Please try again later';
      }

      errors.forEach((errorObject) => {
        if (!errorObject.field) {
          errorObject.field = 'general';
        }

        this.errors[errorObject.field] = `${errorObject.message}`;
      });
    });
  }

  onResetSubmit({ value, valid }: { value: Session, valid: boolean }): void {
    if (!valid) {
      return;
    }

    const params: { token: string, password: string } = {
      token: this.token,
      password: value.password,
    };

    this.UserService.processEmailToken(params).subscribe((): void => {
      this.passwordReset = true;
    }, (error): void => {
      const errors = error.error.error;

      if (error.status === 0 || !errors || errors.length === 0) {
        this.errors.general = 'There is a problem on the server. Please try again later';
        return;
      }

      errors.forEach((errorObject) => {
        if (!errorObject.field) {
          errorObject.field = 'general';
        }

        this.errors[errorObject.field] = `${errorObject.message}`;
      });
    });
  }

  onContinueClick(token: string): void {
    this.Router.navigate(['reset-password'], { queryParams: { token } });
  }

  resetErrors(): void {
    this.errors =  {
      general: '',
      email: '',
      password: '',
      passwordConfirm: '',
    };
  }
}
