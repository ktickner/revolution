import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { NoAuthGuard } from "../shared/guards";

import { CreateAccountComponent } from "./create-account/create-account.component";
import { ForgottenPasswordComponent } from "./forgotten-password/forgotten-password.component";
import { InactiveAccountComponent } from "./inactive-account/inactive-account.component";
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { LoginComponent } from "./login/login.component";
import { UnconfirmedAccountComponent } from "./unconfirmed-account/unconfirmed-account.component";

const userRoutes: Routes = [
  {
    path: 'create-account',
    component: LandingPageComponent,
    canActivate: [NoAuthGuard],
    children: [{ path: '', component: CreateAccountComponent }],
  },
  {
    path: 'forgotten-password',
    component: LandingPageComponent,
    canActivate: [NoAuthGuard],
    children: [{ path: '', component: ForgottenPasswordComponent }],
  },
  {
    path: 'inactive-account',
    component: LandingPageComponent,
    canActivate: [NoAuthGuard],
    children: [{ path: '', component: InactiveAccountComponent }],
  },
  {
    path: 'login',
    component: LandingPageComponent,
    canActivate: [NoAuthGuard],
    children: [{ path: '', component: LoginComponent }],
  },
  {
    path: 'unconfirmed-account',
    component: LandingPageComponent,
    canActivate: [NoAuthGuard],
    children: [{ path: '', component: UnconfirmedAccountComponent }],
  },
  {
    path: 'confirm-account',
    component: LandingPageComponent,
    canActivate: [NoAuthGuard],
    children: [{ path: '', component: UnconfirmedAccountComponent }],
  },
  {
    path: 'activate-account',
    component: LandingPageComponent,
    canActivate: [NoAuthGuard],
    children: [{ path: '', component: InactiveAccountComponent }],
  },
  {
    path: 'reset-password',
    component: LandingPageComponent,
    canActivate: [NoAuthGuard],
    children: [{ path: '', component: ForgottenPasswordComponent }],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes),
  ],
  exports: [
    RouterModule,
  ],
})

export class UserRoutingModule { }
