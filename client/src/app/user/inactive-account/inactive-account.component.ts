import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { Subscription } from "rxjs/Subscription";

import { Errors, Session, User } from "../../shared/interfaces";

import { AuthService, UserService } from "../../shared/services";

@Component({
  selector: 'app-inactive-account',
  templateUrl: './inactive-account.component.html',
  styleUrls: ['./inactive-account.component.scss']
})

export class InactiveAccountComponent implements OnInit, OnDestroy {
  private emailSent: boolean;
  private paramsSub: Subscription;
  private userId: number;
  private token: string;

  private errors: Errors = {
    general: '',
  };

  constructor(
    private ActivatedRoute: ActivatedRoute,
    private Router: Router,
    private AuthService: AuthService,
    private UserService: UserService,
  ) {}

  ngOnInit(): void {
    this.emailSent = false;
    this.paramsSub = this.ActivatedRoute.queryParams.subscribe((params: object): void => {
      this.userId = params['userId'] || 0;
      this.token = params['token'] || '';
    });

    if (this.userId === 0 && this.token === '') {
      this.Router.navigate(['']);
    }

    if (this.token !== '') {
      this.processToken(this.token);
    }
  }

  ngOnDestroy(): void {
    this.paramsSub.unsubscribe();
  }

  onSendClick(id: number): void {
    this.resetErrors();

    const params = {
      id,
      type: 'active',
    };

    this.UserService.sendEmail(params).subscribe((response: Session): void => {
      this.emailSent = true;
      this.token = response.token;
    }, (error): void => {
      if (error.status === 0) {
        this.errors.general = 'There is a problem on the server. Please try again later';
      }

      const errors = error.error.error;

      errors.forEach((errorObject) => {
        if (!errorObject.field) {
          errorObject.field = 'general';
        }

        this.errors[errorObject.field] = `${errorObject.message}`;
      });
    });
  }

  // Super temporary
  onContinueClick(token: string): void {
    this.Router.navigate(['/activate-account'], { queryParams: { token } });
  }

  resetErrors(): void {
    this.errors = {
      general: '',
    };
  }

  processToken(token: string): void {
    const params = {
      token,
    };

    this.UserService.processEmailToken(params).subscribe((response: User): void => {
      this.AuthService.setAuth(response);
      this.Router.navigate(['/feed']);
    }, (error): void => {
      const errors = error.error.error;

      if (error.status === 0 || !errors || errors.length === 0) {
        this.errors.general = 'There is a problem on the server. Please try again later';
        return;
      }

      errors.forEach((errorObject) => {
        if (!errorObject.field) {
          errorObject.field = 'general';
        }

        this.errors[errorObject.field] = `${errorObject.message}`;
      });
    });
  }
}
