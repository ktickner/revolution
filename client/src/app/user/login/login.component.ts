import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';

import { Errors, Session, User } from "../../shared/interfaces";

import { AuthService } from "../../shared/services";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  errors: Errors = {
    general: '',
    email: '',
    password: '',
  };

  validationMessages: Object = {
    email: {
      required: 'Your email address is required',
    },
    password: {
      required: 'Your password is required'
    },
  };

  constructor(
    private FormBuilder: FormBuilder,
    private Router: Router,
    private AuthService: AuthService,
  ) {
    this.loginForm = this.FormBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.loginForm.valueChanges.subscribe((): void => this.onValueChanged(this.loginForm));

    this.onValueChanged(this.loginForm);
  }

  onValueChanged(form: FormGroup): void {
    this.resetErrors();

    for (const field in this.errors) {
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];

        for (const key in control.errors) {
          this.errors[field] += `${messages[key]} `;
        }
      }
    }
  }

  onSubmit({ value, valid }: { value: Session, valid: boolean}): void {
    if (!valid) {
      return;
    }

    this.AuthService.getAuth(value).subscribe((userRecord: User): void => {
      if (userRecord.confirmed === false) {
        this.Router.navigate(['/unconfirmed-account'], { queryParams: { userId: userRecord.id } });
        return;
      }

      if (userRecord.active === false) {
        this.Router.navigate(['/inactive-account'], { queryParams: { userId: userRecord.id } });
        return;
      }

      this.AuthService.setAuth(userRecord);

      this.Router.navigate(['/feed']);
    }, (error): void => {
      if (error.status === 0) {
        this.errors.general = 'There is a problem on the server. Please try again later';
        return;
      }

      error.error.errors.forEach((errorObject) => {
        if (!errorObject.field) {
          errorObject.field = 'general';
        }

        this.errors[errorObject.field] = `${errorObject.message}`;
      });
    })
  }

  resetErrors(): void {
    this.errors = {
      general: '',
      email: '',
      password: '',
    };
  }
}
