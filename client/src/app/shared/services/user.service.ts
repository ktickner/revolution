import { Injectable } from '@angular/core';
import { HttpParams } from "@angular/common/http";

import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { distinctUntilChanged, map } from "rxjs/operators";

import { ApiService } from "./api.service";

import { Session, User } from '../interfaces';

@Injectable()
export class UserService {
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  constructor(
    private ApiService: ApiService,
  ) {}

  setCurrentUser(user: User): void {
    this.currentUserSubject.next(user);
  }

  getCurrentUser(): User {
    return this.currentUserSubject.value;
  }

  getUser(userId: string): Observable<User> {
    const params = { id: userId };

    return this.ApiService.get('user', new HttpParams({ fromObject: params }))
      .pipe(map((response): User => {
        return response.data[0];
      }));
  }

  createUser(user): Observable<User> {
    return this.ApiService.post('user', user)
      .pipe(map((response): User => {
        return response.data[0];
      }));
  }

  updateUser(user: User): Observable<User> {
    return this.ApiService.put('user', { user })
      .pipe(map((response): User => {
        const updatedUser = response.data[0];

        this.currentUserSubject.next(updatedUser);
        return updatedUser;
      }));
  }

  sendEmail(params: object): Observable<Session> {
    return this.ApiService.post('user/send-email', params)
      .pipe(map((response): Session => {
        return response.data[0];
      }));
  }

  processEmailToken(params: object): Observable<User> {
    return this.ApiService.post('user/process-token', params)
      .pipe(map((response): User => {
        return response.data[0];
      }));
  }
}
