import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from "rxjs/observable/ErrorObservable";
import { catchError } from "rxjs/operators";

import { environment } from "../../../environments/environment";

@Injectable()
export class ApiService {
  constructor(
    private HttpClient: HttpClient,
  ) {}

  private formatErrors(error: any) {
    return new ErrorObservable(error);
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.HttpClient.get(
      `${environment.api_url}${path}`,
      { params },
    ).pipe(catchError(this.formatErrors));
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.HttpClient.put(
      `${environment.api_url}${path}`,
      body,
    ).pipe(catchError(this.formatErrors));
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.HttpClient.post(
      `${environment.api_url}${path}`,
      body,
    ).pipe(catchError(this.formatErrors));
  }

  delete(path: string): Observable<any> {
    return this.HttpClient.delete(
      `${environment.api_url}${path}`,
    ).pipe(catchError(this.formatErrors));
  }
}
