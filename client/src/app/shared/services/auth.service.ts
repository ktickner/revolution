import { Injectable } from '@angular/core';

import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { map } from "rxjs/operators";

import { ApiService, JwtService, ProfileService, UserService } from '../services';

import { Session, User } from "../interfaces";

@Injectable()
export class AuthService {
  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  constructor(
    private ApiService: ApiService,
    private JwtService: JwtService,
    private ProfileService: ProfileService,
    private UserService: UserService,
  ) {}

  getAuth(credentials: Session): Observable<User> {
    return this.ApiService.post('session', credentials)
      .pipe(map((response): User => {
        return response.data[0];
      }));
  }

  restoreAuth() {
    return new Promise((resolve, reject) => {
      const token = this.JwtService.getToken();

      if (!token) {
        this.deleteAuth();
        resolve(false);
      }

      this.ApiService.get('session').subscribe((response) => {
        const user = response.data[0];
        user.token = token;

        this.setAuth(user);
        resolve(true);
      }, (error) => {
        this.deleteAuth();
        resolve(false);
      });
    });
  }

  setAuth(user: User): void {
    this.JwtService.saveToken(user.token);
    this.UserService.setCurrentUser(user);
    this.isAuthenticatedSubject.next(true);

    if (user.profile) {
      this.ProfileService.setProfile(user.profile);
    }
  }

  deleteAuth(): void {
    this.JwtService.destroyToken();
    this.UserService.setCurrentUser({} as User);
    this.isAuthenticatedSubject.next(false);
    this.ProfileService.unsetProfile();
  }
}
