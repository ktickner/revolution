import { Injectable } from '@angular/core';
import { HttpParams } from "@angular/common/http";

import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { distinctUntilChanged, map } from "rxjs/operators";

import { ApiService } from "./api.service";

import { Profile } from '../interfaces';

@Injectable()
export class ProfileService {
  private currentProfileSubject = new BehaviorSubject<Profile>({} as Profile);
  public currentProfile = this.currentProfileSubject.asObservable().pipe(distinctUntilChanged());

  private hasProfileSubject = new ReplaySubject<boolean>(1);
  public hasProfile = this.hasProfileSubject.asObservable();

  constructor(
    private ApiService: ApiService,
  ) {
    this.hasProfileSubject.next(false);
  }

  setProfile(profile: Profile): void {
    this.currentProfileSubject.next(profile);
    this.hasProfileSubject.next(true);
  }

  unsetProfile(): void {
    this.currentProfileSubject.next({} as Profile);
    this.hasProfileSubject.next(false);
  }
}
