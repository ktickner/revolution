import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";

import { EqualValidator, ShowAuthedDirective } from "./directives";
import { ListErrorsComponent } from "./list-errors/list-errors.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
  ],
  declarations: [
    EqualValidator,
    ShowAuthedDirective,
    ListErrorsComponent,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    EqualValidator,
    ShowAuthedDirective,
    ListErrorsComponent,
  ],
})

export class SharedModule { }
