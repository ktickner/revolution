import { Address, Image, Genre } from '../interfaces';

export interface Profile {
  id?: string;
  firstName: string;
  lastName: string;
  nickname?: string;
  dob: Date;
  address: Address;
  genres: Array<Genre>;
  image: Image;
}
