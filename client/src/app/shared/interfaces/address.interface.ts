export interface Address {
  id?: string;
  lat: number;
  lng: number;
  display: string;
}
