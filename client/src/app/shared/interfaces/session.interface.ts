export interface Session {
  token?: string;
  email?: string;
  password?: string;
  passwordConfirm?: string;
}
