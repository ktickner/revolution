import { Profile } from '../interfaces';

export interface User {
  email: string;
  id?: number;
  token?: string;
  active?: boolean;
  confirmed?: boolean;
  profile?: Profile;
}
