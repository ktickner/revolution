export interface Image {
  id?: string;
  path: string;
  current?: boolean;
}
