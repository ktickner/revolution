import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';

import { AuthService } from "../services/auth.service";

@Directive({
  selector: '[showAuthed]'
})

export class ShowAuthedDirective {
  constructor(
    private TemplateRef: TemplateRef<any>,
    private ViewContainerRef: ViewContainerRef,
    private AuthService: AuthService,
  ) {}

  condition: boolean;

  ngOnInit() {
    this.AuthService.isAuthenticated.subscribe((isAuthenticated) => {
      if (isAuthenticated === this.condition) {
        this.ViewContainerRef.createEmbeddedView(this.TemplateRef);
        return;
      }

      this.ViewContainerRef.clear();
    });
  }

  @Input() set showAuthed(condition: boolean) {
    this.condition = condition;
  }
}
