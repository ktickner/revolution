import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";

import { Observable } from "rxjs/Observable";
import { take, map } from "rxjs/operators";

import { ProfileService } from "../services";

@Injectable()
export class ProfileGuard implements CanActivate {
  constructor(
    private Router: Router,
    private ProfileService: ProfileService,
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> {
    return this.ProfileService.hasProfile.pipe(
      take(1),
      map((hasProfile) => {
        if (!hasProfile) {
          this.Router.navigate(['/create-profile']);
        }

        return hasProfile || false;
      })
    );
  }
}
