/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NoProfileGuardService } from './no-profile-guard.service';

describe('NoProfileGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NoProfileGuardService]
    });
  });

  it('should ...', inject([NoProfileGuardService], (service: NoProfileGuardService) => {
    expect(service).toBeTruthy();
  }));
});
