import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";

import { Observable } from "rxjs/Observable";
import { take, map } from "rxjs/operators";

import { AuthService } from "../services";

@Injectable()
export class NoAuthGuard implements CanActivate {
  constructor(
    private Router: Router,
    private AuthService: AuthService,
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> {
    return this.AuthService.isAuthenticated.pipe(
      take(1),
      map((isAuth) => {
        if (isAuth) {
          this.Router.navigate(['/feed']);
        }

        return !isAuth;
      }),
    );
  }
}
