import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AuthGuard, NoProfileGuard } from "../shared/guards";

import { LandingPageComponent } from "../user/landing-page/landing-page.component";
import { CreateProfileComponent } from "./create-profile/create-profile.component";

const profileRoutes: Routes = [
  {
    path: 'create-profile',
    component: LandingPageComponent,
    canActivate: [AuthGuard, NoProfileGuard],
    children: [{ path: '', component: CreateProfileComponent }],
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(profileRoutes),
  ],
  exports: [
    RouterModule,
  ],
})

export class ProfileRoutingModule { }
