import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { ProfileService } from "../../shared/services";

import { Errors } from "../../shared/interfaces";

@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.scss']
})

export class CreateProfileComponent implements OnInit {
  private profileForm: FormGroup;

  private errors: Errors = {
    general: '',
    firstName: '',
    lastName: '',
    nickname: '',
    dob: '',
    address: '',
  };

  private validationMessages: object = {
    firstName: {
      required: 'Your first name is required',
    },
    lastName: {
      required: 'Your last name is required',
    },
    nickname: {},
    dob: {
      required: 'Your date of birth is required',
    },
    address: {
      required: 'Your address is required',
    },
  };

  constructor(
    private FormBuilder: FormBuilder,
    private Router: Router,
    private ProfileService: ProfileService,
  ) {
    this.profileForm = this.FormBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      nickname: ['', [Validators.required]],
      dob: ['', [Validators.required]],
      address: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.profileForm.valueChanges.subscribe(data => this.onValueChanged(this.profileForm));
  }

  onValueChanged(form): void {
    for (const field in this.errors) {
      this.errors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];

        for (const key in control.errors) {
          this.errors[field] += `${messages[key]} `;
        }
      }
    }
  }

  onSubmit({ value, valid }) {
    if (!valid) {
      return;
    }
    console.log(value);
  }
}
