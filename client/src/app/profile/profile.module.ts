import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";

import { SharedModule } from "../shared";

import { ProfileRoutingModule } from "./profile-routing.module";

import { CreateProfileComponent } from "./create-profile/create-profile.component";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    ProfileRoutingModule,
  ],
  declarations: [
    CreateProfileComponent,
  ],
  exports: [
    ProfileRoutingModule,
  ],
})
export class ProfileModule { }
