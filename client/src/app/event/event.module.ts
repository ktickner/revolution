import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";

import { SharedModule } from "../shared";

import { EventRoutingModule } from "./event-routing.module";

import { FeedComponent } from "./feed/feed.component";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    EventRoutingModule,
  ],
  declarations: [
    FeedComponent,
  ],
  exports: [
    EventRoutingModule,
  ],
})

export class EventModule { }
