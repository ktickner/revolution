import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AuthGuard, ProfileGuard } from "../shared/guards";

import { FeedComponent } from "./feed/feed.component";

const eventRoutes: Routes = [
  {
    path: 'feed',
    component: FeedComponent,
    canActivate: [AuthGuard, ProfileGuard],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(eventRoutes),
  ],
  exports: [
    RouterModule,
  ],
})

export class EventRoutingModule { }
