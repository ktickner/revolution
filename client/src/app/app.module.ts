import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from "@angular/common/http";

import { AuthGuard, NoAuthGuard, ProfileGuard, NoProfileGuard, HttpTokenInterceptor, HeaderComponent,
  ApiService, AuthService, JwtService, ProfileService, UserService, SharedModule } from "./shared";
import { UserModule } from "./user/user.module";
import { ProfileModule } from "./profile/profile.module";
import { EventModule } from "./event/event.module";

import { AppRoutingModule } from "./app-routing.module";

import { AppComponent } from './app.component';

export function restoreSessionFactory(AuthService: AuthService) {
  return () => {
    return AuthService.restoreAuth();
  };
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    UserModule,
    ProfileModule,
    EventModule,
    AppRoutingModule,
  ],
  providers: [
    { provide: APP_INITIALIZER, useFactory: restoreSessionFactory, deps: [AuthService], multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true },
    AuthGuard,
    NoAuthGuard,
    ProfileGuard,
    NoProfileGuard,
    ApiService,
    AuthService,
    JwtService,
    ProfileService,
    UserService,
  ],
  bootstrap: [AppComponent],
})

export class AppModule { }
