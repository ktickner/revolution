const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const passport = require('passport');

const app = express();

require('./server/config/passport')(passport);

app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200'); // is this the server or the client url?
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-access-token,authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next(null, req);
});

app.use(passport.initialize());

// Require routes into the application
app.use('/api', require('./server/routes'));
app.use('/api/session', require('./server/routes/session'));
app.use('/api/user', require('./server/routes/user'));
app.use('/api/genre', require('./server/routes/genre'));
app.use('/api/profile', require('./server/routes/profile'));

module.exports = app;