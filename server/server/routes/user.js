const passport = require('passport');
const router = require('express').Router();

const UserController = require('../controllers').User;

router.post('', createUser);
router.get('', passport.authenticate('jwt', {session: false}), readUser);
router.put('', passport.authenticate('jwt', { session: false }), updateUser);

router.post('/send-email', sendEmail);
router.post('/process-token', processToken);

async function createUser(req, res) {
  try {
    const user = await UserController.createUser(req.body);
    
    return res.status(200).send(user);
  } catch (err) {
    return res.status(+err.meta.code).send(err);
  }
}

async function readUser(req, res) {
  try {
    const token = req.headers['authorization'].split('Bearer ')[1];
    const user = await UserController.readUser(token);
    
    return res.status(200).send(user);
  } catch (err) {
    return res.status(+err.meta.code).send(err);
  }
}

async function updateUser(req, res) {
  try {
    const token = req.headers['authorization'].split('Bearer ')[1];
    const user = await UserController.updateUser(token, req.body);

    return res.status(200).send(user);
  } catch(err) {
    return res.status(+err.meta.code).send(err);
  }
}

async function sendEmail(req, res) {
  try {
    const user = await UserController.sendEmail(req.body);
  
    return res.status(200).send(user);
  } catch(err) {
    return res.status(+err.meta.code).send(err);
  }
}

async function processToken(req, res) {
  try {
    const user = await UserController.processToken(req.body);
    
    return res.status(200).send(user);
  } catch(err) {
    return res.status(+err.meta.code).send(err);
  }
}

module.exports = router;