const passport = require('passport');
const router = require('express').Router();

const GenreController = require('../controllers').Genre;

router.get('/:name', passport.authenticate('jwt', { session: false }), findGenre);
router.post('', passport.authenticate('jwt', { session: false }), createGenre);


async function findGenre(req, res) {
  const genres = await GenreController.findGenre(req.params);
  return res.status(200).send(genres);
}

async function createGenre(req, res) {
  try {
    const token = req.headers['authorization'].split('Bearer ')[1];
    const genre = await GenreController.createGenre(token, req.body.name);
  
    return res.status(200).send(genre);
  } catch(err) {
    return res.status(+err.meta.code).send(err);
  }
}

module.exports = router;