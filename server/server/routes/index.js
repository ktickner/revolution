const express = require('express');
const router = new express.Router();

router.get('/test', test);

function test (req, res) {
    res.status(200).send({
        message: 'it worked',
    });
}

module.exports = router;