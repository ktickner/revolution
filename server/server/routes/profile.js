const passport = require('passport');
const router = require('express').Router();

const ProfileController = require('../controllers').Profile;

router.post('', passport.authenticate('jwt', { session: false }), createProfile);
router.get('/:id', passport.authenticate('jwt', { session: false }), readProfile);

async function createProfile(req, res) {
  try {
    const token = req.headers['authorization'].split('Bearer ')[1];
    const profile = await ProfileController.createProfile(token, req.body);
    
    return res.status(200).send(profile);
  } catch(err) {
    return res.status(+err.meta.code).send(err);
  }
}

async function readProfile(req, res) {
  try {
    const profile = await ProfileController.readProfile(req.params);
    
    return res.status(200).send(profile);
  } catch(err) {
    return res.status(+err.meta.code).send(err);
  }
}


module.exports = router;