const passport = require('passport');
const router = require('express').Router();

const SessionController = require('../controllers').Session;

router.post('', createSession);
router.get('', passport.authenticate('jwt', {session: false}), getSession);

async function createSession(req, res) {
  try {
    const session = await SessionController.createSession(req.body);
  
    return res.status(200).send(session);
  } catch(err) {
    return res.status(+err.meta.code).send(err);
  }
}

async function getSession(req, res) {
  try {
    const token = req.headers['authorization'].split('Bearer ')[1];
    const session = await SessionController.getSession(token);
    
    return res.status(200).send(session);
  } catch (err) {
    return res.status(+err.meta.code).send(err);
  }
}

module.exports = router;