'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('profile_image', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      profile_id: {
        allowNull: false,
        references: {
          model: 'profile',
          key: 'id',
        },
        type: Sequelize.UUID,
      },
      path: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      current: {
        allowNull: false,
        defaultValue: true,
        type: Sequelize.BOOLEAN
      },
      removed: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deleted_at: {
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('profile_image');
  }
};