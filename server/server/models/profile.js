'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('profile', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
      validate: {
        notEmpty: {
          msg: 'Your first name is required',
        },
      },
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
      validate: {
        notEmpty: {
          msg: 'Your last name is required',
        },
      },
    },
    nickname: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
    },
    date_of_birth: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: '',
      validate: {
        notEmpty: {
          msg: 'Your date of birth is required',
        },
        isDate: {
          msg: 'Incorrect format. Please enter a valid date',
        },
      },
    },
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
  });
  
  Profile.associate = (models) => {
    Profile.belongsTo(models.user);
    Profile.belongsToMany(models.genre, {
      through: models.profile_genre,
    });
    Profile.hasMany(models.profile_address);
    Profile.hasMany(models.profile_image);
  };
  
  return Profile;
};