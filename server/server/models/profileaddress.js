'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProfileAddress = sequelize.define('profile_address', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    profile_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    lat: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: '',
      validate: {
        notEmpty: {
          msg: 'There was an error with the lookup. Please try entering your address again',
        },
        isFloat: {
          msg: 'Address format is invalid. Please try entering your address again',
        },
      },
    },
    lng: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: '',
      validate: {
        notEmpty: {
          msg: 'There was an error with the lookup. Please try entering your address again',
        },
        isFloat: {
          msg: 'Address format is invalid. Please try entering your address again',
        },
      },
    },
    display: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
      validate: {
        notEmpty: {
          msg: 'Your address is required',
        },
      },
    },
    current: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
  });
  
  ProfileAddress.associate = (models) => {
    ProfileAddress.belongsTo(models.profile);
  };
  
  return ProfileAddress;
};