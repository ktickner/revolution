'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProfileImage = sequelize.define('profile_image', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    profile_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    path: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '../assets/gasmasklogooutlinewhite.png',
    },
    current: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    removed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
  });
  
  ProfileImage.associate = (models) => {
    ProfileImage.belongsTo(models.profile);
  };
  
  return ProfileImage;
};