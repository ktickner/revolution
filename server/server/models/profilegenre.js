'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProfileGenre = sequelize.define('profile_genre', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    profile_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    genre_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    removed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
  });
  
  return ProfileGenre;
};