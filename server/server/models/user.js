const bcrypt = require('bcrypt-nodejs');

module.exports = function(sequelize, DataTypes) {
  const User = sequelize.define('user', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: '',
        validate: {
            notEmpty: {
                msg: 'An email must be provided',
            },
            isEmail: {
                msg: 'A valid email is required',
            },
        },
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: '',
        validate: {
            notEmpty: {
                msg: 'A password must be provided',
            },
            len: {
                args: [8],
                msg: 'Password must be at least 8 characters long',
            },
        },
    },
    confirmed: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    active: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
    },
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
    setterMethods: {
        password(value) {
            if (value.length > 7) {
                this.setDataValue('password', bcrypt.hashSync(value, bcrypt.genSaltSync(8), null));
            } else {
                this.setDataValue('password', value);
            }
        },
    },
  });
  
  User.associate = (models) => {
    User.hasOne(models.profile);
    User.hasMany(models.genre, { foreignKey: 'created_by', sourceKey: 'id' });
  };
  
  return User;
};