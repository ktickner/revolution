'use strict';
module.exports = (sequelize, DataTypes) => {
  const Genre = sequelize.define('genre', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    created_by: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'user',
        key: 'id',
      },
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
      validate: {
        notEmpty: {
          msg: 'The genre name is required'
        },
      },
    },
    removed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
  });
  
  Genre.associate = (models) => {
    Genre.belongsTo(models.user, { foreignKey: 'created_by', targetKey: 'id' });
    Genre.belongsToMany(models.profile, {
      through: models.profile_genre,
    });
  };
  
  return Genre;
};