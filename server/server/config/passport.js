const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const User = require('../models').user;

const secret = require('../config').secret;

module.exports = (passport) => {
    // Sessions

    // Used to serialise the user for the session
    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    // Used to deserialize the user
    passport.deserializeUser((id, done) => {
        User.findById(id)
            .then((user) => {
                done(null, user);
            })
            .catch((err) => {
                done(err);
            });
    });

    // JWT login
    passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: secret,
    }, (jwtPayload, next) => {
        User.findOne({ where: { id: jwtPayload.id } })
            .then((user) => {
                if (user) {
                    return next(null, user);
                }

                return next(null, false, {
                    message: 'User account does not exist',
                });
            })
            .catch((error) => {
                return next(error);
            });
    }));
};