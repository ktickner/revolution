const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');

const User = require('../models').user;

const secret = require('../config').secret;

module.exports = {
  async createUser({email, password}) {
    try {
      const existingUser = await User.findOne({ where: { email } });
      
      if (existingUser) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'An account with that email already exists',
              field: 'email',
              value: email,
            }],
          },
        };
      }
      
      const newUser = await User.create({ email, password });
      const token = jwt.sign({ id: newUser.id }, secret);
      
      return {
        meta: {
          success: 'true',
          status: 'OK',
          code: '200',
        },
        data: [{
          id: newUser.id,
          token,
          email: newUser.email,
          confirmed: newUser.confirmed,
          active: newUser.active,
        }],
      };
    } catch (err) {
      throw handleError(err);
    }
  },
  async readUser(token) {
    try {
      const { id } = jwt.verify(token, secret);
      const user = await User.findOne({ where: { id } });
      
      if (!user) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'User does not exist',
              value: id,
            }],
          },
        };
      }
      
      return {
        meta: {
          success: 'true',
          status: 'OK',
          code: '200',
        },
        data: [{
          id: user.id,
          email: user.email,
          confirmed: user.confirmed,
          active: user.active,
        }],
      }
    } catch (err) {
      throw handleError(err);
    }
  },
  async updateUser(token, body) {
    try {
      if (!body.passwordConfirm || body.passwordConfirm === '') {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'Password is required to change account details',
              path: 'passwordConfirm',
              value: body.passwordConfirm,
            }],
          },
        };
      }
      
      if (body.email) {
        const existingUser = await User.findOne({ where: { email: body.email } });
        
        if (existingUser) {
          throw {
            name: 'CustomError',
            error: {
              meta: {
                success: 'false',
                status: 'Bad Request',
                code: '400',
              },
              errors: [{
                type: 'client',
                message: 'An account with that email already exists',
                field: 'email',
                value: body.email,
              }],
            },
          };
        }
      }
      
      const { id } = jwt.verify(token, secret);
      const user = await User.findOne({ where: { id } });
  
      if (!user) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'User does not exist',
              value: id,
            }],
          },
        };
      }
      
      if (!this.validPassword(body.passwordConfirm, user.password)) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'Password is incorrect',
              path: 'passwordConfirm',
              value: body.passwordConfirm,
            }],
          },
        };
      }
      
      const updatedUser = await User.update(body, {
        where: { id },
        returning: true,
      });
      
      if (updatedUser.length === 0 || updatedUser[0] !== 1) {
        throw 'error';
      }
      
      const userRecord = updatedUser[1][0];
  
      return {
        meta: {
          success: 'true',
          status: 'OK',
          code: '200',
        },
        data: [{
          id: userRecord.id,
          email: userRecord.email,
          confirmed: userRecord.confirmed,
          active: userRecord.active,
        }],
      };
    } catch(err) {
      throw handleError(err);
    }
  },
  async sendEmail({ id, email, type }) {
    try {
      if (!id && type !== 'passwordReset') {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'User details are invalid. Please refresh the page and try logging in again',
              field: 'general',
            }],
          },
        };
      }
      
      if (!email && type === 'passwordReset') {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'Email address is required to reset your password',
              field: 'email',
              value: email,
            }],
          },
        };
      }
      
      const query = (type === 'passwordReset' ? { email } : { id });
      const user = await User.findOne({ where: query });
  
      if (!user) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'User does not exist',
              value: id,
            }],
          },
        };
      }
      
      const payload = {
        type,
        id: user.id,
        updatedAt: user.attributes.updated_at,
      };
      
      const token = jwt.sign(payload, secret);
      
      return {
        meta: {
          success: 'true',
          status: 'OK',
          code: '200',
        },
        data: [{
          token,
        }],
      };
    } catch(err) {
      throw handleError(err);
    }
  },
  async processToken({ token, password }) {
    try {
      if (!token) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'Email token is required',
              field: 'general',
            }],
          },
        };
      }
      
      const { id, type } = jwt.verify(token, secret);
      const user = await User.findOne({ where: { id } });
  
      if (!user) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'User does not exist',
              value: id,
            }],
          },
        };
      }
      
      const body = {};
  
      if (type === 'passwordReset') {
        body.password = password;
      } else {
        body[type] = true;
      }
      
      const updatedUser = await User.update(body, {
        where: { id },
        returning: true,
      });
  
      if (updatedUser.length === 0 || updatedUser[0] !== 1) {
        throw 'error';
      }
  
      const userRecord = updatedUser[1][0];
      
      const sessionToken = (type !== 'passwordReset' ? jwt.sign({ id: userRecord.id }, secret) : '');
  
      return {
        meta: {
          success: 'true',
          status: 'OK',
          code: '200',
        },
        data: [{
          token: sessionToken,
          id: userRecord.id,
          email: userRecord.email,
          confirmed: userRecord.confirmed,
          active: userRecord.active,
        }],
      };
    } catch(err) {
      throw handleError(err);
    }
  },
  validPassword(password, passwordRecord) {
    return bcrypt.compareSync(password, passwordRecord);
  },
};

function handleError(err) {
  if (err.name && err.name === 'CustomError') {
    return err.error;
  }
  
  if (err.name && err.name === 'SequelizeValidationError') {
    const errors = [];
    
    errors.push(...err.errors.map((error) => {
      return {
        type: 'client',
        field: error.path,
        value: error.value,
        message: error.message,
      };
    }));
  
    return {
      meta: {
        success: 'false',
        status: 'Bad Request',
        code: '400',
      },
      errors,
    };
  }
  
  return {
    meta: {
      success: 'false',
      status: 'Bad Request',
      code: '500',
    },
    errors: [{
      type: 'server',
      field: 'general',
      message: 'There was a problem on the server. Please try again later',
    }],
  };
}