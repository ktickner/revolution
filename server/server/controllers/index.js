const Genre = require('./genre');
const Profile = require('./profile');
const Session = require('./session');
const User = require('./user');

module.exports = {
  Genre,
  Profile,
  Session,
  User,
};