const jwt = require('jsonwebtoken');

const {
  genre: Genre,
  profile: Profile,
  profile_address: ProfileAddress,
  profile_image: ProfileImage,
  user: User,
} = require('../models');
const UserController = require('./user');

const secret = require('../config').secret;

module.exports = {
  async createSession({ email, password }) {
    try {
      const loginError = {
        name: 'CustomError',
        error: {
          meta: {
            success: 'false',
            status: 'Bad Request',
            code: '400',
          },
          errors: [{
            type: 'client',
            field: 'general',
            message: `Username or password was incorrect`
          }],
        },
      };
      
      if (!email || !password) {
        throw loginError;
      }
      
      const user = await User.findOne({
        where: { email },
        include: [{
          model: Profile,
          required: false,
          include: [{
            model: ProfileAddress,
            required: false,
            where: { current: true },
          }, {
            model: ProfileImage,
            required: false,
            where: { current: true, removed: false },
          }, {
            model: Genre,
            required: false,
            where: { removed: false },
            through: { where: { removed: false } },
          }],
        }],
      });
      
      if (!user || !UserController.validPassword(password, user.password)) {
        throw loginError;
      }
      
      const token = jwt.sign({ id: user.id }, secret);
      
      const userObject = {
        meta: {
          success: 'true',
          status: 'OK',
          code: '200',
        },
        data: [{
          token,
          id: user.id,
          email: user.email,
          confirmed: user.confirmed,
          active: user.active,
        }],
      };
      
      if (user.profile) {
        const genres = user.profile.genres.map((genre) => {
          return {
            id: genre.id,
            name: genre.name,
          };
        });
  
        userObject.data[0].profile = {
          id: user.profile.id,
          firstName: user.profile.first_name,
          lastName: user.profile.last_name,
          nickname: user.profile.nickname,
          dob: user.profile.date_of_birth,
          image: {
            id: user.profile.profile_images[0].id,
            path: user.profile.profile_images[0].path,
          },
          address: {
            id: user.profile.profile_addresses[0].id,
            lat: user.profile.profile_addresses[0].lat,
            lng: user.profile.profile_addresses[0].lng,
            display: user.profile.profile_addresses[0].display,
          },
          genres,
        };
      }
      
      return userObject;
    } catch(err) {
      throw handleError(err);
    }
  },
  async getSession(token) {
    try {
      const { id } = jwt.verify(token, secret);
      const user = await User.findOne({
        where: { id },
        include: [{
          model: Profile,
          required: false,
          include: [{
            model: ProfileAddress,
            required: false,
            where: { current: true },
          }, {
            model: ProfileImage,
            required: false,
            where: { current: true, removed: false },
          }, {
            model: Genre,
            required: false,
            where: { removed: false },
            through: { where: { removed: false } },
          }],
        }],
      });
      
      if (!user) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'User does not exist',
              value: id,
            }],
          },
        };
      }
      
      const userObject = {
        meta: {
          success: 'true',
          status: 'OK',
          code: '200',
        },
        data: [{
          token,
          id: user.id,
          email: user.email,
          confirmed: user.confirmed,
          active: user.active,
        }],
      };
  
      if (user.profile) {
        const genres = user.profile.genres.map((genre) => {
          return {
            id: genre.id,
            name: genre.name,
          };
        });
    
        userObject.data[0].profile = {
          id: user.profile.id,
          firstName: user.profile.first_name,
          lastName: user.profile.last_name,
          nickname: user.profile.nickname,
          dob: user.profile.date_of_birth,
          image: {
            id: user.profile.profile_images[0].id,
            path: user.profile.profile_images[0].path,
          },
          address: {
            id: user.profile.profile_addresses[0].id,
            lat: user.profile.profile_addresses[0].lat,
            lng: user.profile.profile_addresses[0].lng,
            display: user.profile.profile_addresses[0].display,
          },
          genres,
        };
      }
  
      return userObject;
    } catch(err) {
      throw handleError(err);
    }
  },
};

function handleError(err) {
  if (err.name && err.name === 'CustomError') {
    return err.error;
  }
  
  return {
    meta: {
      success: 'false',
      status: 'Internal Server Error',
      code: '500',
    },
    errors: [{
      type: 'server',
      field: 'general',
      message: 'There was a problem on the server. Please try again later',
    }],
  };
}