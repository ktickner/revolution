const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const jwt = require('jsonwebtoken');

const Genre = require('../models').genre;
const User = require('../models').user;

const secret = require('../config').secret;

module.exports = {
  async createGenre(token, name) {
    try {
      const { id } = jwt.verify(token, secret);
      const user = await User.findOne({ where: { id } });
  
      if (!user) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'User does not exist',
              value: id,
            }],
          },
        };
      }
      
      const genreName = (name ? name.toLowerCase() : '');
      const existingGenre = await Genre.findOne({ where: { name: genreName } });
      
      if (existingGenre) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'Genre already exists',
              value: genreName,
              field: 'genre',
            }],
          },
        };
      }
      
      const genre = await Genre.create({ name: genreName, created_by: user.id });
      return {
        meta: {
          success: 'true',
          status: 'OK',
          code: '200',
        },
        data: [{
          id: genre.id,
          name: genre.name,
          removed: genre.removed,
        }],
      };
    } catch(err) {
      throw handleError(err);
    }
  },
  async findGenre({ name }) {
    const genres = await Genre.findAll({
      where: {
        name: {
          [Op.iLike]: `${name}%`,
        },
        removed: false,
      },
    }).map((genre) => {
      return {
        id: genre.id,
        name: genre.name,
        removed: genre.removed,
      };
    });
  
    return {
      meta: {
        success: 'true',
        status: 'OK',
        code: '200',
      },
      data: genres,
    };
  },
};

function handleError(err) {
  if (err.name && err.name === 'CustomError') {
    return err.error;
  }
  
  if (err.name && err.name === 'SequelizeValidationError') {
    const errors = [];
    
    errors.push(...err.errors.map((error) => {
      return {
        type: 'client',
        field: error.path,
        value: error.value,
        message: error.message,
      };
    }));
    
    return {
      meta: {
        success: 'false',
        status: 'Bad Request',
        code: '400',
      },
      errors,
    };
  }
  
  return {
    meta: {
      success: 'false',
      status: 'Bad Request',
      code: '500',
    },
    errors: [{
      type: 'server',
      field: 'general',
      message: 'There was a problem on the server. Please try again later',
    }],
  };
}