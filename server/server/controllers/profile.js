const jwt = require('jsonwebtoken');

const {
  sequelize,
  genre: Genre,
  profile: Profile,
  profile_genre: ProfileGenre,
  profile_address: ProfileAddress,
  profile_image: ProfileImage,
  user: User
} = require('../models');

const secret = require('../config').secret;

module.exports = {
  async createProfile(token, body) {
    let transaction;
    
    try {
      const { id: user_id } = jwt.verify(token, secret);
      const user = await User.findOne({ where: { id: user_id } });
      
      if (!user) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'User does not exist',
              value: id,
            }],
          },
        };
      }
      
      const existingProfile = await Profile.findOne({ where: { user_id } });
      
      if (existingProfile) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'User account already has a profile',
              field: 'user_id',
              value: user_id,
            }],
          },
        };
      }
      
      transaction = await sequelize.transaction();
      
      const first_name = (body.firstName ? body.firstName.toLowerCase() : '');
      const last_name = (body.lastName ? body.lastName.toLowerCase() : '');
      const nickname = (body.nickname ? body.nickname.toLowerCase() : '');
      
      const newProfile = {
        user_id,
        first_name,
        last_name,
        nickname,
        date_of_birth: body.dob,
      };
      const newProfileAddress = body.address;
      const newProfileGenres = body.genres;
      
      const profile = await Profile.create(newProfile, { transaction });
      const profile_id = profile.id;
      const newProfileImage = { profile_id };
  
      newProfileAddress.profile_id = profile_id;
      
      const profileImage = await ProfileImage.create(newProfileImage, { transaction });
      const profileAddress = await ProfileAddress.create(newProfileAddress, { transaction });
      const profileGenres = await Promise.all(newProfileGenres.map(async (newProfileGenre) => {
        const newGenre = {
          profile_id,
          genre_id: newProfileGenre.id,
        };
        
        const profileGenre = await ProfileGenre.create(newGenre, { transaction });
        const genre = await Genre.findOne({ where: { id: profileGenre.genre_id } });
        
        return {
          id: genre.id,
          name: genre.name,
        };
      }));
      
      await transaction.commit();
      
      return {
        id: profile.id,
        userId: profile.user_id,
        firstName: profile.first_name,
        lastName: profile.last_name,
        nickname: profile.nickname,
        dob: profile.date_of_birth,
        image: {
          id: profileImage.id,
          path: profileImage.path,
        },
        address: {
          id: profileAddress.id,
          lat: profileAddress.lat,
          lng: profileAddress.lng,
          display: profileAddress.display,
        },
        genres: profileGenres,
      };
    } catch(err) {
      if (transaction) {
        await transaction.rollback();
      }
      
      throw handleError(err);
    }
  },
  async readProfile({ id }) {
    try {
      const profile = await Profile.findOne({
        where: { id },
        include: [{
          model: ProfileAddress,
          required: false,
          where: { current: true },
        }, {
          model: ProfileImage,
          required: false,
          where: { current: true, removed: false },
        }, {
          model: Genre,
          required: false,
          where: { removed: false },
          through: { where: { removed: false } },
        }],
      });
      
      if (!profile) {
        throw {
          name: 'CustomError',
          error: {
            meta: {
              success: 'false',
              status: 'Bad Request',
              code: '400',
            },
            errors: [{
              type: 'client',
              message: 'Profile does not exist',
              value: id,
            }],
          },
        };
      }
      
      const genres = profile.genres.map((genre) => {
        return {
          id: genre.id,
          name: genre.name,
        };
      });
      
      return {
        meta: {
          success: 'true',
          status: 'OK',
          code: '200',
        },
        data: [{
          id: profile.id,
          firstName: profile.first_name,
          lastName: profile.last_name,
          nickname: profile.nickname,
          dob: profile.date_of_birth,
          image: {
            id: profile.profile_images[0].id,
            path: profile.profile_images[0].path,
          },
          address: {
            id: profile.profile_addresses[0].id,
            lat: profile.profile_addresses[0].lat,
            lng: profile.profile_addresses[0].lng,
            display: profile.profile_addresses[0].display,
          },
          genres,
        }],
      };
    } catch(err) {
      throw handleError(err);
    }
  },
};

function handleError(err) {
  console.log(err);
  if (err.name && err.name === 'CustomError') {
    return err.error;
  }
  
  if (err.name && err.name === 'SequelizeValidationError') {
    const errors = [];
    
    errors.push(...err.errors.map((error) => {
      return {
        type: 'client',
        field: error.path,
        value: error.value,
        message: error.message,
      };
    }));
    
    return {
      meta: {
        success: 'false',
        status: 'Bad Request',
        code: '400',
      },
      errors,
    };
  }
  
  return {
    meta: {
      success: 'false',
      status: 'Bad Request',
      code: '500',
    },
    errors: [{
      type: 'server',
      field: 'general',
      message: 'There was a problem on the server. Please try again later',
    }],
  };
}