# Client

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.21.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Updating Packages
Using the npm-check-updates package for dependency management. Run ncu to view out of date packages. Run ncu -u to update package.json, then run npm install to update.
