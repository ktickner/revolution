/* Angular Imports */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

/* Module Imports */
import { SharedModule } from '../shared/shared.module';
import { AccountModule } from './account/account.module';
import { ProfileModule } from './profile/profile.module';
import { SessionModule } from './session/session.module';

// Routing Module
import { UserRoutingModule } from './user-routing.module';

/* Component Imports */
import { UserComponent } from './user.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AccountModule,
    ProfileModule,
    SessionModule,
    SharedModule,
    UserRoutingModule,
  ],
  declarations: [
    UserComponent,
  ],
  // Might have to export sub modules
})

export class UserModule { }
