/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DestroySessionComponent } from './destroy-session.component';

describe('DestroySessionComponent', () => {
  let component: DestroySessionComponent;
  let fixture: ComponentFixture<DestroySessionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestroySessionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestroySessionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
