/* Angular Imports */
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-destroy-session',
  templateUrl: './destroy-session.component.html',
  styleUrls: ['./destroy-session.component.scss']
})

export class DestroySessionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
