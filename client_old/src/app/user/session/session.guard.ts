import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { SessionService } from './session.service';

@Injectable()
export class SessionGuard implements CanActivate {
  constructor(
    private router: Router,
    private SessionService: SessionService,
  ) {}

  canActivate() {
    if (this.SessionService.getSession()) {
      return true;
    }

    this.router.navigate(['/login']);
    return false;
  }
}
