/* Angular Imports */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/* Component Imports */

const sessionRoutes: Routes = [];

@NgModule({
  imports: [
    RouterModule.forChild(sessionRoutes),
  ],
  exports: [
    RouterModule,
  ],
})

export class SessionRoutingModule { }
