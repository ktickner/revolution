/* Angular Imports */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

/* Module Imports */
// Routing Module
import { SessionRoutingModule } from './session-routing.module';

/* Component Imports */
import { SessionComponent } from './session.component';
import { CreateSessionComponent } from './create-session/create-session.component';
import { DestroySessionComponent } from './destroy-session/destroy-session.component';
import { SessionFormComponent } from './session-form/session-form.component';
import { InactiveAccountComponent } from './inactive-account/inactive-account.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SessionRoutingModule,
  ],
  declarations: [
    SessionComponent,
    CreateSessionComponent,
    DestroySessionComponent,
    SessionFormComponent,
    InactiveAccountComponent
  ],
})

export class SessionModule { }
