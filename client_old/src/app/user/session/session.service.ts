/* Angular Imports */
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

/* Service Imports */
import { HttpRequestConfigService } from "../../shared/http-request-config.service";

@Injectable()
export class SessionService {
  private  base_url: string;
  public token: string;

  constructor(
    private Http: Http,
    private HttpRequestConfigService: HttpRequestConfigService,
  ) {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
    this.base_url = `${this.HttpRequestConfigService.getBaseUrl()}/session`;
  }

  getSession() {
    // Endpoint: revolution/rest/session/:token
    // Method: GET
    // Do I need this?
    // Would send the sessionToken in cookie/localStorage to the server to return a boolean

    // Checks for session in service - if not checks for session in localStorage
    // if so checks token expiry
    // if not expired returns true, else returns false
    return true;
  }

  createSession(userInput): Observable<boolean> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.Http.post(this.base_url, JSON.stringify(userInput), options)
      .map((response: Response) => {
        const responseJson = response.json();
        const token = responseJson && responseJson.meta.success === 'true' && responseJson.data[0].attributes.token;

        if (token) {
          this.token = token;
          localStorage.setItem('currentUser', JSON.stringify({ email: userInput.email, token }));

          // I may need to set a User property to the UserService

          return true;
        }

        return false;
      });
  }

  destroySession(): void {
    // Endpoint: revolution/rest/session/:token
    // Method: DELETE
    // Removes session token from cookies/localStorage
    // Don't forgot I'll need to remove the user property from the userService as well
    // Does the onInit in DestroySessionComponent need to double check everything has been deleted properly?
    this.token = null;
    localStorage.removeItem('currentUser');
    // is this really it?!? see above...
  }

  sendActivationEmail() {
    // this will need the user's id
    // this will need to send the email
  }
}
