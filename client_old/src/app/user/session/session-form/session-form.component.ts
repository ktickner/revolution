/* Angular Imports */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

/* Interface Imports */
import { User } from '../../user.interface';

/* Service Imports */
import { UserService } from '../../user.service';
import { SessionService } from '../session.service';

@Component({
  selector: 'app-session-form',
  templateUrl: './session-form.component.html',
  styleUrls: ['./session-form.component.scss'],
  providers: [
    UserService,
    SessionService,
  ],
})

export class SessionFormComponent implements OnInit {
  loading: boolean = false;

  sessionForm: FormGroup;

  formErrors: Object = {
    email: '',
    password: '',
    general: '',
  };

  validationMessages: Object = {
    email: {
      required: 'We can\'t log you in without your email',
    },
    password: {
      required: 'Your password let\'s us know you are who you say you are',
    },
  };

  constructor(
    private FormBuilder: FormBuilder,
    private Router: Router,
    private UserService: UserService,
    private SessionService: SessionService,
  ) { }

  ngOnInit() {
    this.SessionService.destroySession();
    this.sessionForm = this.FormBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });

    this.sessionForm.valueChanges
      .subscribe(data => this.onValueChanged());

    this.onValueChanged();
  }

  onValueChanged() {
    const form = this.sessionForm;

    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];

        for (const key in control.errors) {
          this.formErrors[field] += `${messages[key]} `;
        }
      }
    }
  }

  onSubmit({ value, valid }: { value: User, valid: boolean }) {
    console.log(value, valid);

    if (valid) {
      this.loading = true;
      this.createSession(value);
    }
  }

  createSession(userInput) {
    this.SessionService.createSession(userInput)
      .subscribe((result) => {
        console.log('result', result);
        if (result === true) {
          this.Router.navigate(['/feed']);
        } else {
          this.formErrors['general'] = 'Username or Password was incorrect';
          this.loading = false;
        }
      }, (error) => {
        console.log('error', error);
        const errorResponse = error.json();
        console.log(errorResponse);
        this.formErrors['general'] = errorResponse.error[0].message;
        this.loading = false;
      });
  }
}
