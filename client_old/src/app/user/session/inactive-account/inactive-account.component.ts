/* Angular Imports */
import { Component, OnInit } from '@angular/core';

/* Service Imports */
import { SessionService } from '../session.service';

@Component({
  selector: 'app-inactive-account',
  templateUrl: './inactive-account.component.html',
  styleUrls: ['./inactive-account.component.scss'],
  providers: [SessionService],
})
export class InactiveAccountComponent implements OnInit {
  emailSent: boolean;

  constructor(
    private SessionService: SessionService,
  ) { }

  ngOnInit() {
    this.emailSent = false;
  }

  onSendEmailClick() {
    this.SessionService.sendActivationEmail();
    this.emailSent = true;
  }
}
