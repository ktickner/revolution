import { User, Profile } from './user.interface';

export const VALID_USERS: Array<User> = [
  {
    email: 'email@example.com',
    password: 'password',
  },
  {
    email: 'email2@example.com',
    password: 'password',
  },
  {
    email: 'email3@example.com',
    password: 'password',
  },
  {
    email: 'email4@example.com',
    password: 'password',
  },
];

export const PROFILE: Profile = {
  id: 1,
  firstName: 'John',
  lastName: 'Dawson',
  nickname: 'Johnny',
  gender: 'Male',
  DOB: '14th March 1996',
  bio: 'Fcukin bitches, gettin money',
  location: 'My house',
  genres: '',
  picture: '../assets/gasmaskforlogothicklines.jpg',
};
