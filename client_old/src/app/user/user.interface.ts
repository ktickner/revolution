export interface User {
  email: string;
  password: string;
  passwordConfirm?: string;
}

export interface Profile {
  id: Number;
  firstName: string;
  lastName: string;
  nickname?: string;
  gender: string;
  DOB: string;
  bio?: string;
  location: string;
  genres: string;
  picture: string;
}
