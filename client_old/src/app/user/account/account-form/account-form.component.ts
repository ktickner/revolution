/* Angular Imports */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.scss']
})

export class AccountFormComponent implements OnInit {
  accountForm: FormGroup;

  formErrors: Object = {
    email: '',
    newPassword: '',
    newPasswordConfirm: '',
    currentPassword: '',
    general: '',
  };

  validationMessages: Object = {
    email: {},
    newPassword: {
      minlength: 'Your password needs to be at least 8 characters long',
    },
    newPasswordConfirm: {
      validateEqual: 'Your passwords don\'t match',
    },
    currentPassword: {
      required: 'Your current password is required to change these settings',
    },
    general: {},
  };

  constructor(
    private FormBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.accountForm = this.FormBuilder.group({
      email: ['', []],
      newPassword:  ['', [Validators.minLength(8)]],
      newPasswordConfirm: ['', []],
      currentPassword: ['', [Validators.required]],
    });

    this.accountForm.valueChanges
      .subscribe(data => this.onValueChanged());

    this.onValueChanged();
  }

  onValueChanged() {
    const form = this.accountForm;

    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];

        for (const key in control.errors) {
          this.formErrors[field] += `${messages[key]} `;
        }
      }
    }
  }

  onSubmit({ value, valid }: { value: Object, valid: boolean }) {
    console.log(value, valid);
  }
}
