/* Angular Imports */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/* Component Imports */
import { EditAccountComponent } from './edit-account/edit-account.component';

const accountRoutes: Routes = [
  { path: 'account/:id/edit', component: EditAccountComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(accountRoutes),
  ],
  exports: [
    RouterModule,
  ],
})

export class AccountRoutingModule { }
