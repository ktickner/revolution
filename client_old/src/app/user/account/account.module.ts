/* Angular Imports */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

/* Module Imports */
// Routing Module
import { AccountRoutingModule } from './account-routing.module';

/* Component Imports */
import { AccountComponent } from './account.component';
import { CreateAccountComponent } from './create-account/create-account.component';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { ForgottenPasswordComponent } from './forgotten-password/forgotten-password.component';
import { AccountFormComponent } from './account-form/account-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AccountRoutingModule,
  ],
  declarations: [
    AccountComponent,
    CreateAccountComponent,
    ForgottenPasswordComponent,
    EditAccountComponent,
    AccountFormComponent,
  ],
})

export class AccountModule { }
