import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-forgotten-password',
  templateUrl: 'forgotten-password.component.html',
  styleUrls: ['forgotten-password.component.scss']
})
export class ForgottenPasswordComponent implements OnInit {
  forgottenPasswordForm: FormGroup;
  emailSent: boolean;

  constructor(
    private FormBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.emailSent = false;
    this.forgottenPasswordForm = this.FormBuilder.group({
      email: ['', Validators.required],
    });
  }

  onSubmit({ value, valid}: { value: Object, valid: boolean }) {
    console.log(value, valid);

    if (valid) {
      // UserService.forgottenPassword
      this.emailSent = true;
    }
  }
}
