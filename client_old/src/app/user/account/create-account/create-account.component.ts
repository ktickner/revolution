/* Angular Imports */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

/* Interface Imports */
import { User } from '../../user.interface';

/* Service Imports */
import { AccountService } from '../account.service';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss'],
  providers: [AccountService],
})

export class CreateAccountComponent implements OnInit {
  loading: boolean = false;

  accountForm: FormGroup;

  formErrors: Object = {
    email: '',
    password: '',
    passwordConfirm: '',
    general: '',
  };

  validationMessages: Object = {
    email: {
      required: 'Please enter your email',
    },
    password: {
      required: 'Please enter a password',
      minlength: 'Your password needs to be at least 8 characters long',
    },
    passwordConfirm: {
      required: 'Please confirm your password',
      validateEqual: 'Your passwords don\'t match',
    },
  };

  constructor(
    private FormBuilder: FormBuilder,
    private Router: Router,
    private AccountService: AccountService,
  ) { }

  ngOnInit() {
    this.accountForm = this.FormBuilder.group({
      email: ['', [Validators.required]],
      password:  ['', [
        Validators.required,
        Validators.minLength(8),
      ]],
      passwordConfirm: ['', [Validators.required]],
    });

    this.accountForm.valueChanges
      .subscribe(data => this.onValueChanged());

    this.onValueChanged();
  }

  onValueChanged() {
    const form = this.accountForm;

    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];

        for (const key in control.errors) {
          this.formErrors[field] += `${messages[key]} `;
        }
      }
    }
  }

  onSubmit({ value, valid }: { value: User, valid: boolean }) {
    if (valid) {
      this.loading = true;
      this.createAccount(value);
    }
  }

  createAccount(userInput) {
    this.AccountService.createAccount(userInput)
      .subscribe((result) => {
        console.log('result', result);
        if (result === true) {
          this.Router.navigate(['/profile/create']);
        } else {
          this.formErrors['general'] = 'Username or Password was incorrect';
          this.loading = false;
        }
      }, (error) => {
        const errorResponse = error.json();
        const path = errorResponse.error[0].path || 'general';
        this.formErrors[path] = errorResponse.error[0].message;
        this.loading = false;
      });
  }
}
