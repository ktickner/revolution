import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class AccountService {
  private  base_url = 'http://localhost:8000/api/user';

  constructor(private Http: Http) { }

  getAccount() {
    // Endpoint: revolution/rest/account/:id
    // Method: GET
  }

  createAccount(userInput): Observable<boolean> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.Http.post(this.base_url, JSON.stringify(userInput), options)
      .map((response: Response) => {
        const responseJson = response.json();
        const token = responseJson && responseJson.meta.success === 'true' && responseJson.data[0].attributes.token;

        if (token) {
          // Need to import the session service... or do I? if i call the get session and have that return this.token it should populate in the constructor?
          // if not maybe return the token and set it in the component
          // this.token = token;
          localStorage.setItem('currentUser', JSON.stringify({ email: userInput.email, token }));

          // I may need to set a User property to the UserService

          return true;
        }

        return false;
      });
  }

  updateAccount() {
    // Endpoint: revolution/rest/account/:id
    // Method: PUT
  }

  destroyAccount() {
    // Endpoint: revolution/rest/account/:id
    // Method: DELETE
    // Pretty sure I can utilise the DELETE method for semantics but use it to toggle the active column in the user record
  }

  forgottenPassword() {
    // Endpoint: revolution/rest/user/:id - *cough* no...
    // Method: PUT
    // Edit endpoint to add the password reset token to the user record
  }
}
