/* Angular Imports */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/* Component Imports */
import { SessionComponent } from './session/session.component';
import { CreateSessionComponent } from './session/create-session/create-session.component';
import { DestroySessionComponent } from './session/destroy-session/destroy-session.component';
import { CreateAccountComponent } from './account/create-account/create-account.component';
import { ForgottenPasswordComponent } from './account/forgotten-password/forgotten-password.component';
import { InactiveAccountComponent } from "./session/inactive-account/inactive-account.component";

const userRoutes: Routes = [
  {
    path: 'login',
    component: SessionComponent,
    children: [
      { path: '', component: CreateSessionComponent },
    ],
  },
  {
    path: 'logout',
    component: SessionComponent,
    children: [
      { path: '', component: DestroySessionComponent },
    ],
  },
  {
    path: 'register',
    component: SessionComponent,
    children: [
      { path: '', component: CreateAccountComponent },
    ],
  },
  {
    path: 'forgotten-password',
    component: SessionComponent,
    children: [
      { path: '', component: ForgottenPasswordComponent },
    ],
  },
  {
    path: 'inactive-account',
    component: SessionComponent,
    children: [
      { path: '', component: InactiveAccountComponent },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes),
  ],
  exports: [
    RouterModule,
  ],
})

export class UserRoutingModule { }
