/* Angular Imports */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/* Component Imports */
import { CreateProfileComponent } from './create-profile/create-profile.component';
import { ShowProfileComponent } from './show-profile/show-profile.component';

const userRoutes: Routes = [
  { path: 'profile/create', component: CreateProfileComponent },
  { path: 'profile/:id', component: ShowProfileComponent, },
  { path: 'my-profile', component: ShowProfileComponent, },
];

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes),
  ],
  exports: [
    RouterModule,
  ],
})

export class ProfileRoutingModule { }
