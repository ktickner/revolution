/* Angular Imports */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

/* Interface Imports */
import { Profile } from '../../user.interface';

@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.scss']
})

export class CreateProfileComponent implements OnInit {
  profileForm: FormGroup;
  activeSection: String;

  formErrors: Object = {
    firstName: '',
    lastName: '',
    nickname: '',
    gender: '',
    DOB: '',
    bio: '',
    location: '',
    genres: '',
    picture: '',
  };

  validationMessages: Object = {
    firstName: {
      required: 'Please enter your first name',
    },
    lastName: {
      required: 'Please enter your last name',
    },
    gender: {
      required: 'Please select your gender',
    },
    DOB: {
      required: 'Please enter your date of birth',
    },
    location: {
      required: 'Please enter your address',
    },
    genres: {
      required: 'Please enter your favourite genres',
    },
    picture: {
      required: 'Please choose a profile picture',
    },
  };

  constructor(
    private FormBuilder: FormBuilder,
    private Router: Router,
  ) { }

  ngOnInit() {
    this.profileForm = this.FormBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      nickname: [''],
      gender: ['', Validators.required],
      DOB: ['', Validators.required],
      bio: [''],
      location: ['', Validators.required],
      genres: ['', Validators.required],
      picture: ['', Validators.required],
    });

    this.activeSection = 'details';

    this.profileForm.valueChanges
      .subscribe(data => this.onValueChanged());

    this.onValueChanged();
  }

  onValueChanged() {
    const form = this.profileForm;

    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];

        for (const key in control.errors) {
          this.formErrors[field] += `${messages[key]} `;
        }
      }
    }
  }

  onSubmit({ value, valid }: { value: Profile, valid: boolean }) {
    console.log(value, valid);

    if (valid) {
      // UserService.createProfile.then
      console.log('profile created');
      this.Router.navigate(['/feed']);
      //.catch throw error
    }
  }

  changeSection(section: String): void {
    this.activeSection = section;
  }
}
