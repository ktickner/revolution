import { Injectable } from '@angular/core';

import { PROFILE } from '../mock-users';
import { Profile } from '../user.interface';

@Injectable()
export class ProfileService {

  constructor() { }

  getProfile(profileId) {
    // Endpoint: revolution/rest/profile/:id
    // Method: GET

    // All very temporary
    return Promise.resolve(PROFILE)
      .then((profileRecord: Profile) => {
        const profileArray = [profileRecord];
        return profileArray.find(profile => profile.id === profileId);
      });
  }

  createProfile() {
    // Endpoint: revolution/rest/profile
    // Method: POST
  }

  updateProfile() {
    // Endpoint: revolution/rest/profile/:id
    // Method: PUT
  }

  destroyProfile() {
    // Endpoint: revolution/rest/profile/:id
    // Method: DELETE
  }
}
