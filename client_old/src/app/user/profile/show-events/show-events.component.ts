import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-events',
  templateUrl: './show-events.component.html',
  styleUrls: ['./show-events.component.scss']
})
export class ShowEventsComponent implements OnInit {
  collapsable: boolean = true;
  collapsed: boolean = false;

  constructor() { }

  ngOnInit() {
    if (this.collapsable) {
      this.collapsed = true;
    }
  }

  onEventToggle(collapsed: boolean): void {
    if (!this.collapsable) {
      return;
    }

    console.log('parent', collapsed);
    this.collapsed = collapsed;
  }
}
