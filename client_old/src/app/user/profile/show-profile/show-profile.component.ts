/* Angular Imports */
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';

/* Interface Imports */
import { Profile } from '../../user.interface';

/* Service Imports */
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-show-profile',
  templateUrl: './show-profile.component.html',
  styleUrls: ['./show-profile.component.scss'],
  providers: [ProfileService],
})

export class ShowProfileComponent implements OnInit {
  activeSection: String = 'info';
  profile: Profile = {
    id: null,
    firstName: '',
    lastName: '',
    nickname: '',
    gender: '',
    DOB: '',
    bio: '',
    location: '',
    genres: '',
    picture: '',
  };

  constructor(
    private ProfileService: ProfileService,
    private Router: Router,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    const path = this.ActivatedRoute.snapshot.url[0].path;
    const profileId = (path === 'my-profile' ? 1 : +this.ActivatedRoute.snapshot.params['id']);
    this.getProfile(profileId)
      .then((profile) => {
        if (profile === undefined) {
          this.Router.navigate(['/404']);
        }

        this.profile = profile;
      });
  }

  getProfile(profileId) {
    return this.ProfileService.getProfile(profileId);
  }

  changeSection(section) {
    console.log(section);
    this.activeSection = section;
  }
}
