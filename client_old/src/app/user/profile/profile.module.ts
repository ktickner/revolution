/* Angular Imports */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

/* Module Imports */
import { EventModule } from '../../event/event.module';

// Routing Module
import { ProfileRoutingModule } from './profile-routing.module';

/* Component Imports */
import { ProfileComponent } from './profile.component';
import { CreateProfileComponent } from './create-profile/create-profile.component';
import { ShowProfileComponent } from './show-profile/show-profile.component';
import { ShowDetailsComponent } from './show-details/show-details.component';
import { ShowEventsComponent } from './show-events/show-events.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EventModule,
    ProfileRoutingModule,
  ],
  declarations: [
    ProfileComponent,
    CreateProfileComponent,
    ShowProfileComponent,
    ShowDetailsComponent,
    ShowEventsComponent,
  ],
})

export class ProfileModule { }
