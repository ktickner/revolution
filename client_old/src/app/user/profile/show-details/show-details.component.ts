/* Angular Imports */
import { Component, OnInit, Input } from '@angular/core';

/* Interface Imports */
import { Profile } from '../../user.interface';

@Component({
  selector: 'app-show-details',
  templateUrl: './show-details.component.html',
  styleUrls: ['./show-details.component.scss']
})

export class ShowDetailsComponent implements OnInit {
  @Input() profile: Profile;

  constructor() { }

  ngOnInit() {
  }
}
