/* Angular Imports */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

/* Component Imports */
import { SharedComponent } from './shared.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { NotFoundComponent } from './not-found/not-found.component';

/* Directive Imports */
import { EqualValidator } from './equal-validator.directive';

/* Service Imports */
import { HttpRequestConfigService } from "./http-request-config.service";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [
    EqualValidator,
    SharedComponent,
    TopBarComponent,
    NotFoundComponent,
  ],
  providers: [
    HttpRequestConfigService,
  ],
  exports: [
    EqualValidator,
    TopBarComponent,
  ],
})

export class SharedModule { }
