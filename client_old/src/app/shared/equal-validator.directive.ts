/* Angular Imports */
import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[validateEqual][formControlName],[validateEqual][formControl],[validateEqual][ngModal]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => EqualValidator), multi: true },
  ],
})

export class EqualValidator implements Validator {

  constructor(
    @Attribute('validateEqual') public validateEqual: string,
    @Attribute('reverse') public reverse: string,
  ) { }

  private get isReverse() {
    if (!this.reverse) return false;
    return this.reverse === 'true' ? true : false;
  }

  validate(formControl: AbstractControl): { [key: string]: any } {
    // form control value (e.g. passwordConfirm)
    let controlValue = formControl.value;

    // form control to be compared (e.g. password)
    let compareControl = formControl.root.get(this.validateEqual);

    // value not equal
    if (compareControl && controlValue !== compareControl.value && !this.isReverse) return {
      validateEqual: false
    };

    // value not equal and reverse
    if (compareControl && controlValue !== compareControl.value && this.isReverse) {
      compareControl.setErrors({ validateEqual: false });
    }

    // value equal and reverse
    if (compareControl && controlValue === compareControl.value && this.isReverse) {
      delete compareControl.errors['validateEqual'];
      if (!Object.keys(compareControl.errors).length) compareControl.setErrors(null);
    }

    return null;
  }
}
