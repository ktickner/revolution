import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})

export class TopBarComponent implements OnInit {

  constructor(
    private Router: Router,
  ) { }

  ngOnInit() {
  }

  onCreateEventClick() {
    this.Router.navigate(['/event/create']);
  }

  onLogoutClick() {
    // This will destroy the current session when they're implemented
    this.Router.navigate(['']);
  }
}
