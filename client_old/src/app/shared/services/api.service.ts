/* Angular Imports */
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { HttpHeaders, HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/* Service Imports */
import { JwtService } from "./jwt.service";

@Injectable()
export class ApiService {
  constructor(
    private HttpClient: HttpClient,
    private JwtService: JwtService,
  ) {}

  private setHeaders(): HttpHeaders {
    const headersConfig = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };

    if (this.JwtService.getToken()) {
      headersConfig['Authorisation'] = `Token ${this.JwtService.getToken()}`;
    }

    return new HttpHeaders(headersConfig);
  }

  private formatErrors(error: any): Observable<any> {
    return Observable.throw(error.json());
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.HttpClient.get(
      `${environment.api_url}${path}`,
      { headers: this.setHeaders(), params },
      ).catch(this.formatErrors);
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.HttpClient.put(
      `${environment.api_url}${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders() },
    ).catch(this.formatErrors);
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.HttpClient.post(
      `${environment.api_url}${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders() },
    ).catch(this.formatErrors);
  }

  delete(path: string): Observable<any> {
    return this.HttpClient.delete(
      `${environment.api_url}${path}`,
      { headers: this.setHeaders() },
    ).catch(this.formatErrors);
  }
}
