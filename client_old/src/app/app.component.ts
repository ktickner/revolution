/* Angular Imports */
import { Component, ViewEncapsulation } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class AppComponent {
  public topBar: boolean;

  constructor(
    private Router: Router,
  ) {
    Router.events.subscribe((routeEvent) => {
      if (routeEvent instanceof NavigationEnd) {
        this.topBar = !this.isProtectedRoute(routeEvent.urlAfterRedirects);
      }
    });
  }

  isProtectedRoute(routeUrl) {
    const publicRoutes = ['/login', '/logout', '/register', '/forgotten-password', '/profile/create'];
    return publicRoutes.find(route => routeUrl === route);
  }
}
