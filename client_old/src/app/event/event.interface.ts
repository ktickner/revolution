export interface Event {
  id?: number;
  name: string;
  author: EventAuthor;
  location: string;
  description: string;
  startDatetime: string;
  finishDatetime?: string;
  overEighteen: boolean;
  isPrivate: boolean;
  image: string;
  feedRank?: number;
  genres?: Array<EventGenre>;
  comments?: Array<EventComment>;
}

export interface EventAuthor {
  id: Number;
  firstName: String;
  lastName: String;
  nickname: String;
}
export interface EventGenre {
  id: Number;
  name: String;
}

export interface EventComment {
  id: Number;
  comment: String;
  author: CommentUser;
}

export interface CommentUser {
  id: Number;
  name: String;
  profilePicture: String;
}
