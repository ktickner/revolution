/* Angular Imports */
import { Component, OnInit } from '@angular/core';

/* Interface Imports */
import { Event } from '../event.interface';

/* Service Imports */
import { EventService } from '../event.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss'],
  providers: [EventService],
})

export class FeedComponent implements OnInit {
  eventFeed: Array<Event>;

  constructor(
    private EventService: EventService,
  ) { }

  ngOnInit(): void {
    this.eventFeed = [];

    this.getEventFeed()
      .then((feed: Array<Event>): void => {
        this.eventFeed = feed;
      });
  }

  getEventFeed(): Promise<Array<Event>> {
    return this.EventService.getFeed();
  }
}
