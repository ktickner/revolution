/* Angular Imports */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

/* Module Imports */

// Routing Module
import { EventRoutingModule } from './event-routing.module';

/* Component Imports */
import { EventComponent } from './event.component';
import { FeedComponent } from './feed/feed.component';
import { SharedModule } from '../shared/shared.module';
import { ShowEventComponent } from './show-event/show-event.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { EventFormComponent } from './event-form/event-form.component';
import { EditEventComponent } from './edit-event/edit-event.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EventRoutingModule,
    SharedModule,
  ],
  declarations: [
    EventComponent,
    FeedComponent,
    ShowEventComponent,
    CreateEventComponent,
    EventFormComponent,
    EditEventComponent,
  ],
  exports: [
    ShowEventComponent,
  ]
})

export class EventModule { }
