import { Injectable } from '@angular/core';

import { Event } from './event.interface';
import { EVENTS } from './mock-events';

@Injectable()
export class EventService {
  eventFeed: Array<Event>;

  constructor() { }

  getEvent(eventId): Promise<Event | undefined> {
    // Endpoint: revolution/rest/event/:id
    // Method: GET

    // All very temporary
    return Promise.resolve(EVENTS)
      .then((eventRecords: Array<Event>) => {
        return eventRecords.find(event => event.id === eventId);
      });
  }

  createEvent() {

  }

  updateEvent() {

  }

  destroyEvent() {

  }

  getFeed(): Promise<Array<Event>> {
    // Endpoint: revolution/rest/event/feed/:userId
    // Method: GET
    // Will need the userId to determine the feed order

    // Will need some sort of authentication... there's probably a global way to handle this
    return Promise.resolve(EVENTS)
      .then((feedRecord: Array<Event>) => {
        this.eventFeed = feedRecord;

        return this.eventFeed;
      });
  }
}
