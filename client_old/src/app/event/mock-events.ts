import { Event } from './event.interface';

export const EVENTS: Array<Event> = [
  {
    id: 1,
    name: 'An Event',
    author: {
      id: 1,
      firstName: 'An',
      lastName: 'Author',
      nickname: 'Scrambles the Deathdealer',
    },
    location: 'Ya mum\'s place',
    description: 'Party at ya mum\'s place. Bitches invited.',
    startDatetime: '14/01/1969, 12:00am',
    finishDatetime: '14/01/1979, 12:01pm',
    overEighteen: true,
    isPrivate: false,
    image: '../assets/gasmaskforlogothicklines.jpg',
    feedRank: 1,
    genres: [
      {
        id: 1,
        name: 'Metal',
      },
      {
        id: 2,
        name: 'Speed Metal',
      },
      {
        id: 3,
        name: 'Thrash Metal',
      },
    ],
    comments: [
      {
        id: 1,
        comment: 'Holy fuck it\'s an event!',
        author: {
          id: 1,
          name: 'Smithson',
          profilePicture: '../assets/gasmaskforlogothicklines.jpg',
        },
      },
      {
        id: 1,
        comment: 'Holy fuck it\'s a comment!',
        author: {
          id: 1,
          name: 'Paul Smith',
          profilePicture: '../assets/gasmaskforlogothicklines.jpg',
        },
      },
      {
        id: 1,
        comment: 'Y\'all motherfuckers be cray cray',
        author: {
          id: 1,
          name: 'Casey Jordan',
          profilePicture: '../assets/gasmaskforlogothicklines.jpg',
        },
      },
    ],
  },
  {
    id: 2,
    name: 'Another Event',
    author: {
      id: 1,
      firstName: 'An',
      lastName: 'Author',
      nickname: 'Scrambles the Deathdealer',
    },
    location: 'Ya mum\'s place',
    description: 'Party at ya mum\'s place. Bitches invited.',
    startDatetime: '14/01/1969, 12:00am',
    finishDatetime: '14/01/1979, 12:01pm',
    overEighteen: true,
    isPrivate: false,
    image: '../assets/gasmaskforlogothicklines.jpg',
    feedRank: 10,
    genres: [
      {
        id: 1,
        name: 'Metal',
      },
      {
        id: 2,
        name: 'Speed Metal',
      },
      {
        id: 3,
        name: 'Thrash Metal',
      },
    ],
  },
  {
    id: 3,
    name: 'A Third Event',
    author: {
      id: 1,
      firstName: 'An',
      lastName: 'Author',
      nickname: 'Scrambles the Deathdealer',
    },
    location: 'Ya mum\'s place',
    description: 'Party at ya mum\'s place. Bitches invited.',
    startDatetime: '14/01/1969, 12:00am',
    finishDatetime: '14/01/1979, 12:01pm',
    overEighteen: true,
    isPrivate: false,
    image: '../assets/gasmaskforlogothicklines.jpg',
    feedRank: 25,
    genres: [
      {
        id: 1,
        name: 'Metal',
      },
      {
        id: 2,
        name: 'Speed Metal',
      },
      {
        id: 3,
        name: 'Thrash Metal',
      },
    ],
  },
  {
    id: 4,
    name: 'Look at all these events',
    author: {
      id: 1,
      firstName: 'Another',
      lastName: 'Author',
      nickname: '',
    },
    location: 'Ya mum\'s place',
    description: 'And they\'re all at ya mum\'s place.',
    startDatetime: '14/01/1969, 12:00am',
    finishDatetime: '14/01/1979, 12:01pm',
    overEighteen: true,
    isPrivate: false,
    image: '../assets/gasmaskforlogothicklines.jpg',
    feedRank: 22,
    genres: [
      {
        id: 1,
        name: 'Metal',
      },
      {
        id: 2,
        name: 'Speed Metal',
      },
      {
        id: 3,
        name: 'Thrash Metal',
      },
    ],
  },
];
