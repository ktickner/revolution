/* Angular Imports */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/* Component Imports */
import { FeedComponent } from './feed/feed.component';
import { ShowEventComponent } from './show-event/show-event.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { EditEventComponent } from './edit-event/edit-event.component';

const eventRoutes: Routes = [
  { path: 'feed', component: FeedComponent },
  { path: 'event/create', component: CreateEventComponent },
  { path: 'event/:id/edit', component: EditEventComponent },
  { path: 'event/:id', component: ShowEventComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(eventRoutes),
  ],
  exports: [
    RouterModule,
  ],
})

export class EventRoutingModule { }
