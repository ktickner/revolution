import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss']
})

export class EventFormComponent implements OnInit {
  eventForm: FormGroup;

  formErrors: Object = {
    name: '',
    location: '',
    description: '',
    startDatetime: '',
    finishDatetime: '',
    overEighteen: '',
    isPrivate: '',
    image: '',
    genres: '',
  };

  validationMessages: Object = {
    name: {
      required: 'Please enter a name for the event',
    },
    location: {
      required: 'Please enter a location for the event',
    },
    description: {
      required: 'Please enter a description for the event',
    },
    startDatetime: {
      required: 'Please enter a starting date and time for the event',
    },
    finishDatetime: {},
    overEighteen: {
      required: 'Please specify if the event is 18+',
    },
    isPrivate: {
      required: 'Please specify if the event is private',
    },
    image: {},
    genres: {
      required: 'Please enter which music genres will be played',
    },
  };

  constructor(
    private FormBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.eventForm = this.FormBuilder.group({
      name: ['', Validators.required],
      location: ['', Validators.required],
      description: ['', Validators.required],
      startDatetime: ['', Validators.required],
      finishDatetime: [''],
      overEighteen: ['', Validators.required],
      isPrivate: ['', Validators.required],
      image: [''],
      genres: ['', Validators.required],
    });

    this.eventForm.valueChanges
      .subscribe(data => this.onValueChanged());

    this.onValueChanged();
  }

  onValueChanged() {
    const form = this.eventForm;

    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];

        for (const key in control.errors) {
          this.formErrors[field] += `${messages[key]} `;
        }
      }
    }
  }

  onSubmit({ value, valid }: { value: Object, valid: boolean}) {
    console.log(value, valid);
  }
}
