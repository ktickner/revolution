/* Angular Imports */
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';

/* Interface Imports */
import { Event } from '../event.interface';

/* Service Imports */
import { EventService } from '../event.service';

@Component({
  selector: 'app-show-event',
  templateUrl: './show-event.component.html',
  styleUrls: ['./show-event.component.scss'],
  providers: [EventService],
})

export class ShowEventComponent implements OnInit {
  @Input() collapsable: boolean;
  @Output() onToggle = new EventEmitter();
  event: Event;
  author: String;
  collapsed: boolean = false;

  constructor(
    private EventService: EventService,
    private Router: Router,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    if (this.collapsable) {
      this.collapsed = true;
    }

    // const eventId = +this.ActivatedRoute.snapshot.params['id'];
    // this.getEvent(eventId)
    //   .then((event: Event | undefined): void => {
    //     if (event === undefined) {
    //       this.Router.navigate(['/404']);
    //     }
    //
    //     this.event = event;
    //     this.author = event.author.nickname !== '' ? event.author.nickname : `${event.author.firstName} ${event.author.lastName}`;
    //   });
  }

  getEvent(eventId: Number): Promise<Event | undefined>{
    return this.EventService.getEvent(eventId);
  }

  onEventClick(collapsed): void {
    if (!this.collapsable) {
      return;
    }

    console.log('child', collapsed);

    this.collapsed = !collapsed;
    this.onToggle.emit(this.collapsed);
  }
}
