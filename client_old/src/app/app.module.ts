/* Angular Imports */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

/* Module Imports */
import { SharedModule } from './shared/shared.module';
import { UserModule } from './user/user.module';
import { EventModule } from './event/event.module';

// Routing Module
import { AppRoutingModule } from './app-routing.module';

/* Component Imports */
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    SharedModule,
    UserModule,
    EventModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})

export class AppModule { }
